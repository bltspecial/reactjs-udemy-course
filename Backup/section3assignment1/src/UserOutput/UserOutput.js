import React, {Component} from 'react';
import { generateKeyPair } from 'crypto';

class UserOutput extends Component {
    render(){
        const inputStyle = {
            backgroundColor: 'gray'
        }

        return(
            <div>
                <p style={inputStyle}>Username: {this.props.Username}</p>
                <p>2st Paragraph</p>
            </div>
        )
    } 
}

export default UserOutput;