import React, {Component} from 'react';
import './App.css';

import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component {
  state = {
    UserName: 'Bob'
  }

  changeUserNameHandler = (event) => {
    this.setState(
      { UserName: event.target.value }
    )
  }

  render(){
    return (
      <div className="App">
        <UserInput UserName={this.state.UserName} ChangeUserName={this.changeUserNameHandler} />
        <UserOutput Username={this.state.UserName}/>
        <UserOutput Username={this.state.UserName} />
      </div>
    );
  }
}

export default App;
