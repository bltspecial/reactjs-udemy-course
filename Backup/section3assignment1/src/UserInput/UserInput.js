import React, {Component} from 'react';
import UserInputStyles from './UserInput.css'

class UserInput extends Component {
    render(){
        return(
            <div className="UserInput">
                <input type="input" onChange={this.props.ChangeUserName} value={this.props.UserName}/>
            </div>
        )
    }
}

export default UserInput;