import React, {Component} from 'react';
import './App.css';
import './Char/Char.css'

import Validation from './Validation/Validation';
import Character from './Char/Char';

class App extends Component {
  state = {text: ''};

  inputChangedHandler = (event) => {
    this.setState({text: event.target.value});
  }

  deleteCharacterHandler = (index) => {
    const charArray = [...this.state.text.split('')];
    charArray.splice(index, 1);
    this.setState({text: charArray.join("")});
  }

  render() {
    return (
      <div className="App">
        <input type="text" onChange={(event) => this.inputChangedHandler(event)} value={this.state.text} />
        <p>Length: {this.state.text.length}</p>
        <Validation textlength={this.state.text.length}/>
        {this.state.text.split("").map((char, index) => { 
          return <Character character={char} key={index} click={() => this.deleteCharacterHandler(index)} />
        })}
      </div>
    )
  }
}

export default App;
