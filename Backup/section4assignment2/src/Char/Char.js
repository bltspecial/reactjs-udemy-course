import React from 'react';

const character = (props) => {
    return (
        <div className="Character" onClick={props.click}>
            {props.character}
        </div>
    );
}

export default character;