import React, { Component } from 'react';

class Course extends Component {
    componentDidMount(){
        console.log(this.props);
    }

    getCourseTitle = () => {
        var queryParams = [];
        var query = new URLSearchParams(this.props.location.search);
        for (let queryParam of query.entries())
            queryParams.push(queryParam);
        return queryParams[0][1];
    }

    render () {
        let courseDisplay = <h1>Please select a course</h1>;
        if (this.props.location.search){
            courseDisplay = (
                <div>
                    <h1>{this.getCourseTitle()}</h1>
                    <p>You selected the Course with ID: {this.props.match.params.id}</p>
                </div>
            );
        }
            
        return (
            <div>
                {courseDisplay}
            </div>
        );
    }
}

export default Course;